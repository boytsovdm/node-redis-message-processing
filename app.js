var redis = require('redis');
var	client = require('redis-connection')();

// check for errors
var input = process.argv[2];
if ( input === 'getErrors' ) {
	client.lrange('errors', 0, -1, function(err, errors) {
		if ( errors.length === 0 )
			return;

		console.log( errors );
		client.del('errors');
	});
}

var repeat_times = 10;
var wait = 500;

// used to flushdb after each test
var dev = false;

// unique id for currenly running app.js
var id = Math.random().toString(36).substr(2, 9);

var sub = require('redis-connection')('subscriber'),
	pub = redis.createClient();

var counter = null;

// flag to make flushdb only once with first app start
client.exists('start_flush', function(err, reply) {
	if ( reply === 0 ) {
		client.flushdb();
		client.set('start_flush', 'done');
	}
});

// flush in the end of generator time
function end_flush() {
	if ( dev !== true )
		return;

	client.flushdb();
}

// save app id
client.hsetnx('apps', id, 'active');

// save current app as generator, if it's not started
client.setnx('generator', id);
var generator = null;

client.get('generator', function(err, generator) {
	if ( generator != id )
		return;

	start_generator();
});

function start_generator() {
	client.set('generator', id);

	client.get('counter', function(err, counter) {
		var index = 1;

		if ( counter !== null )
			index = counter;

		var generator = interval( function() {
			// if ( index % 100 === 0 )
				console.log('iteration ' + index);

			pub.publish('messages', Math.random().toString(36).substring(2, 9));
			client.set('counter', index++ );
			client.set('last_message', + new Date());

			// set working timestamp
		}, wait, repeat_times - index + 1);
	});



	setTimeout( function() {
		client.del('generator_restart');
	}, 1000);
}

interval( function() {
	client.get('counter', function(err, counter) {
		if ( counter === null )
			return;

		if ( counter < repeat_times ) {
			// generator check (should be time-zone independent)
			client.get('last_message', function(err, ts) {
				cur_ts = + new Date();

				// start another generator, if there are no update for 1 s,
				if ( ts !== null && (cur_ts - ts) > 1000 ) {
					client.exists('generator_restart', function(err, reply) {
						if ( reply === 1 )
							return;

						console.log('start_generator ' + counter);
						client.set('generator_restart', true);
						start_generator(true);
					});
				}
			});
		}
		// shutdown
		else {
			end_flush();

			// make time for events to finish their events, then quit
			// 10s is default time to finish processing all messages
			// but more time needed, if message processing will
			// get behind message publishing (on lower "wait" var)
			setTimeout( function() {
				sub.quit();
				pub.quit();
				client.quit();
			}, 10000);
		}
	});
}, 500, repeat_times + 1); // for var wait 500
// }, wait * 10, repeat_times / 10 + 10); // for lower var wait

sub.subscribe('messages');

sub.on('message', function (channel, message) {
	client.get('counter', function(err, counter) {
		if ( counter === null )
			return;

		// save current message as processed by running app, if not
		// already processed by other app
		client.hsetnx('messages', counter, id);

		// simulate error
		var chance = Math.random();
		if (chance < 0.05) {
			if ( dev === true )
				console.log('error!');

			client.rpush('errors', [counter, id]);
		}
	});
});

// check messages
interval( function() {
	client.hgetall('messages', function(err, messages) {
		if ( messages === null || messages === undefined )
			return;

		client.get('generator', function(err, generator) {
			if ( generator === null )
				return;

			var this_app = generator === id
				? ' (active generator)'
				: '';
			var length = Object.keys(messages).length;
			var last = messages[(length - 1).toString()];
			if ( typeof last !== 'undefined' )
				console.log('id: ' + id + this_app + ', ' + length + ' msgs, last: ' + last);
		});

	});
}, 500, repeat_times + 1); // for var wait 500
// }, wait * 50, repeat_times / 50); // for lower var wait

function interval(func, wait, times){
	var interv = function(w, t) {
		return function() {
			if ( typeof t === "undefined" || t-- > 0 ) {
				setTimeout(interv, w);
				try {
					func.call(null);
				}
				catch(e) {
					t = 0;
					throw e.toString();
				}
			}
		};
	} (wait, times);

	setTimeout(interv, wait);
};